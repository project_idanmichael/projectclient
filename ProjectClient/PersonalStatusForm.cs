﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectClient
{
    public partial class PersonalStatusForm : Form
    {
        private DialogResult res;
        public PersonalStatusForm(string numGames,string rightAnswers,string wrongAnswers,string avgAnswers)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            this.numGamesLbl.Text += numGames;
            this.rightAnsLbl.Text += rightAnswers;
            this.wrongAndLbl.Text += wrongAnswers;
            this.avgLbl.Text += avgAnswers;
        }

        private void PersonalStatusForm_Load(object sender, EventArgs e)
        {
            this.res = DialogResult.Abort;
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.res = DialogResult.OK;
            this.Close();
        }

        private void PersonalStatusForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
        }
    }
}
