﻿namespace ProjectClient
{
    partial class InGameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BackgroundPanel = new System.Windows.Forms.Panel();
            this.Anwer3Btn = new System.Windows.Forms.Button();
            this.Answer2Btn = new System.Windows.Forms.Button();
            this.Answer1Btn = new System.Windows.Forms.Button();
            this.Answer0Btn = new System.Windows.Forms.Button();
            this.QuestionLbl = new System.Windows.Forms.Label();
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.BackBtn = new System.Windows.Forms.Button();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.TimeLeftLbl = new System.Windows.Forms.Label();
            this.MyScoreLbl = new System.Windows.Forms.Label();
            this.QuestionsLeftLbl = new System.Windows.Forms.Label();
            this.BackgroundPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackgroundPanel
            // 
            this.BackgroundPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.BackgroundPanel.Controls.Add(this.Anwer3Btn);
            this.BackgroundPanel.Controls.Add(this.Answer2Btn);
            this.BackgroundPanel.Controls.Add(this.Answer1Btn);
            this.BackgroundPanel.Controls.Add(this.Answer0Btn);
            this.BackgroundPanel.Controls.Add(this.QuestionLbl);
            this.BackgroundPanel.Location = new System.Drawing.Point(65, 154);
            this.BackgroundPanel.Name = "BackgroundPanel";
            this.BackgroundPanel.Size = new System.Drawing.Size(703, 534);
            this.BackgroundPanel.TabIndex = 31;
            // 
            // Anwer3Btn
            // 
            this.Anwer3Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.Anwer3Btn.FlatAppearance.BorderSize = 0;
            this.Anwer3Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Anwer3Btn.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Anwer3Btn.ForeColor = System.Drawing.Color.LightGreen;
            this.Anwer3Btn.Location = new System.Drawing.Point(0, 445);
            this.Anwer3Btn.Name = "Anwer3Btn";
            this.Anwer3Btn.Size = new System.Drawing.Size(703, 57);
            this.Anwer3Btn.TabIndex = 3;
            this.Anwer3Btn.Text = "The Answer is something very long sometimes";
            this.Anwer3Btn.UseVisualStyleBackColor = false;
            this.Anwer3Btn.Click += new System.EventHandler(this.Answer_Click);
            // 
            // Answer2Btn
            // 
            this.Answer2Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.Answer2Btn.FlatAppearance.BorderSize = 0;
            this.Answer2Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Answer2Btn.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Answer2Btn.ForeColor = System.Drawing.Color.LightGreen;
            this.Answer2Btn.Location = new System.Drawing.Point(0, 339);
            this.Answer2Btn.Name = "Answer2Btn";
            this.Answer2Btn.Size = new System.Drawing.Size(703, 57);
            this.Answer2Btn.TabIndex = 2;
            this.Answer2Btn.Text = "The Answer is something very long sometimes";
            this.Answer2Btn.UseVisualStyleBackColor = false;
            this.Answer2Btn.Click += new System.EventHandler(this.Answer_Click);
            // 
            // Answer1Btn
            // 
            this.Answer1Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.Answer1Btn.FlatAppearance.BorderSize = 0;
            this.Answer1Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Answer1Btn.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Answer1Btn.ForeColor = System.Drawing.Color.LightGreen;
            this.Answer1Btn.Location = new System.Drawing.Point(0, 233);
            this.Answer1Btn.Name = "Answer1Btn";
            this.Answer1Btn.Size = new System.Drawing.Size(703, 57);
            this.Answer1Btn.TabIndex = 1;
            this.Answer1Btn.Text = "The Answer is something very long sometimes";
            this.Answer1Btn.UseVisualStyleBackColor = false;
            this.Answer1Btn.Click += new System.EventHandler(this.Answer_Click);
            // 
            // Answer0Btn
            // 
            this.Answer0Btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(0)))));
            this.Answer0Btn.FlatAppearance.BorderSize = 0;
            this.Answer0Btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Answer0Btn.Font = new System.Drawing.Font("Tahoma", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Answer0Btn.ForeColor = System.Drawing.Color.LightGreen;
            this.Answer0Btn.Location = new System.Drawing.Point(0, 127);
            this.Answer0Btn.Name = "Answer0Btn";
            this.Answer0Btn.Size = new System.Drawing.Size(703, 57);
            this.Answer0Btn.TabIndex = 0;
            this.Answer0Btn.Text = "The Answer is something very long sometimes";
            this.Answer0Btn.UseVisualStyleBackColor = false;
            this.Answer0Btn.Click += new System.EventHandler(this.Answer_Click);
            // 
            // QuestionLbl
            // 
            this.QuestionLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuestionLbl.Font = new System.Drawing.Font("Segoe UI", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(190)))), ((int)(((byte)(255)))), ((int)(((byte)(50)))));
            this.QuestionLbl.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.QuestionLbl.Location = new System.Drawing.Point(3, 0);
            this.QuestionLbl.Name = "QuestionLbl";
            this.QuestionLbl.Size = new System.Drawing.Size(697, 111);
            this.QuestionLbl.TabIndex = 999;
            this.QuestionLbl.Text = "WHO WAS THE PRESIDENT OF THE FUCKING UNITED STATES OF AMERICA";
            this.QuestionLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 29;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BackBtn
            // 
            this.BackBtn.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackBtn.Location = new System.Drawing.Point(658, 109);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(163, 38);
            this.BackBtn.TabIndex = 5;
            this.BackBtn.Text = "Exit Game";
            this.BackBtn.UseVisualStyleBackColor = true;
            this.BackBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.ForeColor = System.Drawing.Color.White;
            this.UserNameLbl.Location = new System.Drawing.Point(9, 109);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(143, 28);
            this.UserNameLbl.TabIndex = 33;
            this.UserNameLbl.Text = "Name of User";
            // 
            // TimeLeftLbl
            // 
            this.TimeLeftLbl.AutoSize = true;
            this.TimeLeftLbl.Font = new System.Drawing.Font("Cambria", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TimeLeftLbl.ForeColor = System.Drawing.Color.Red;
            this.TimeLeftLbl.Location = new System.Drawing.Point(400, 109);
            this.TimeLeftLbl.Name = "TimeLeftLbl";
            this.TimeLeftLbl.Size = new System.Drawing.Size(32, 34);
            this.TimeLeftLbl.TabIndex = 34;
            this.TimeLeftLbl.Text = "8";
            // 
            // MyScoreLbl
            // 
            this.MyScoreLbl.AutoSize = true;
            this.MyScoreLbl.Font = new System.Drawing.Font("Cambria", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyScoreLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.MyScoreLbl.Location = new System.Drawing.Point(69, 694);
            this.MyScoreLbl.Name = "MyScoreLbl";
            this.MyScoreLbl.Size = new System.Drawing.Size(120, 34);
            this.MyScoreLbl.TabIndex = 35;
            this.MyScoreLbl.Text = "Score: 0";
            // 
            // QuestionsLeftLbl
            // 
            this.QuestionsLeftLbl.Font = new System.Drawing.Font("Cambria", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionsLeftLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.QuestionsLeftLbl.Location = new System.Drawing.Point(511, 691);
            this.QuestionsLeftLbl.Name = "QuestionsLeftLbl";
            this.QuestionsLeftLbl.Size = new System.Drawing.Size(255, 34);
            this.QuestionsLeftLbl.TabIndex = 36;
            this.QuestionsLeftLbl.Text = "Questions Left: 0";
            this.QuestionsLeftLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // InGameForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 737);
            this.Controls.Add(this.QuestionsLeftLbl);
            this.Controls.Add(this.MyScoreLbl);
            this.Controls.Add(this.TimeLeftLbl);
            this.Controls.Add(this.BackgroundPanel);
            this.Controls.Add(this.HeadlineLbl);
            this.Controls.Add(this.BackBtn);
            this.Controls.Add(this.UserNameLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InGameForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "InGameForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InGameForm_FormClosing);
            this.Load += new System.EventHandler(this.InGameForm_Load);
            this.BackgroundPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel BackgroundPanel;
        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Button BackBtn;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Label QuestionLbl;
        private System.Windows.Forms.Button Answer1Btn;
        private System.Windows.Forms.Button Answer0Btn;
        private System.Windows.Forms.Button Anwer3Btn;
        private System.Windows.Forms.Button Answer2Btn;
        private System.Windows.Forms.Label TimeLeftLbl;
        private System.Windows.Forms.Label MyScoreLbl;
        private System.Windows.Forms.Label QuestionsLeftLbl;
    }
}