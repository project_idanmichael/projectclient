﻿/** 
-- PROTOCOL 666: Sending the key and iv of the aes to the server for proper communication --
    SYNTAX: [666][KeyLength : 2][Key : KeyLength][IVLength : 2][IV : IVLength]
**/


using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Security.Cryptography;

namespace ProjectClient
{
    static class Crypto
    {
        private static NetworkStream _clientStream;
        private static RijndaelManaged _cryptoManager;
        private static ICryptoTransform _encryptor;
        private static ICryptoTransform _decryptor;

        /// gets the key of the crypto
        public static byte[] Key
        {
            get
            {
                // checking that crypto was initiated
                if(Crypto._cryptoManager == null)
                {
                    throw new Exception("ERROR : Please Initiate Crypto");
                }
                return Crypto._cryptoManager.Key;
            }
        }
        public static byte[] IV
        {
            get
            {
                // checking that crypto was initiated
                if (Crypto._cryptoManager == null)
                {
                    throw new Exception("ERROR : Please Initiate Crypto");
                }
                return Crypto._cryptoManager.IV;
            }
        }

        public static void CryptoInit(NetworkStream clientStream)
        {
            byte[] msg = new byte[128];
            byte[] temp;
            int curLength = 0;
            Crypto._clientStream = clientStream;

            // initiating the encrytion manager and encryption details
            Crypto._cryptoManager = new RijndaelManaged();
            // creating encrypt and decrypt transformer
            Crypto._encryptor = Crypto._cryptoManager.CreateEncryptor(Crypto._cryptoManager.Key, Crypto._cryptoManager.IV);
            Crypto._decryptor = Crypto._cryptoManager.CreateDecryptor(Crypto._cryptoManager.Key, Crypto._cryptoManager.IV);

            /* creating the message */
            // adding the protocl and key
            temp = Encoding.UTF8.GetBytes("666" + Helper.GetPaddedNumber(_cryptoManager.Key.Length, 2));
            Buffer.BlockCopy(temp, 0, msg, 0, 5);
            curLength += 5;
            Buffer.BlockCopy(Crypto._cryptoManager.Key, 0, msg, curLength, _cryptoManager.Key.Length);
            curLength += _cryptoManager.Key.Length;
            // adding the iv
            temp = Encoding.UTF8.GetBytes(Helper.GetPaddedNumber(_cryptoManager.IV.Length, 2));
            Buffer.BlockCopy(temp, 0, msg, curLength, 2);
            curLength += 2;
            Buffer.BlockCopy(_cryptoManager.IV, 0, msg, curLength, _cryptoManager.IV.Length);
            
            // Sending the message
            Crypto._clientStream.Write(msg, 0, curLength);
            Crypto._clientStream.Flush();
        }

        /* Encryption */
        private static byte[] EncryptBase64(byte[] plainText)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            return encoder.GetBytes(Convert.ToBase64String(plainText));
        }
        private static byte[] EncryptAES(byte[] plainText)
        {
            // checking that crypto was initiated
            if (Crypto._cryptoManager == null)
            {
                throw new Exception("ERROR : Please Initiate Crypto");
            }
            // gets a stream to write all the data directly into the memory
            MemoryStream memStream = new MemoryStream();
            // uses that stream for the encryption
            CryptoStream Stream = new CryptoStream(memStream, Crypto._encryptor, CryptoStreamMode.Write);
            // encrypting the data, moves to the memory stream
            Stream.Write(plainText, 0, plainText.Length);
            Stream.Flush();
            // removing data from the encryption steam for safety
            if (Crypto._cryptoManager != null)
            {
                Crypto._cryptoManager.Clear();
            }
            Stream.Close();
            // returning array
            return memStream.ToArray();
        }

        /* Decryption */
        private static byte[] DecryptBase64(byte[] encText)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            return Convert.FromBase64String(encoder.GetString(encText));
        }
        private static byte[] DecryptAES(byte[] encText)
        {
            byte[] finalValue;
            // checking that crypto was initiated
            if (Crypto._cryptoManager == null)
            {
                throw new Exception("ERROR : Please Initiate Crypto");
            }
            // gets a stream to write all the data directly into the memory
            MemoryStream memStream = new MemoryStream(encText);
            // uses that stream for the encryption =>
            CryptoStream Stream = new CryptoStream(memStream, Crypto._decryptor, CryptoStreamMode.Read);
            // decrypting the data, moves to the memory stream
            // ( the buffer, offset, length )
            StreamReader streamReader = new StreamReader(Stream);
            finalValue = new UTF8Encoding().GetBytes(streamReader.ReadToEnd());

            // removing data from the encryption steam for safety
            if (Crypto._cryptoManager != null)
            {
                Crypto._cryptoManager.Clear();
            }
            // closing stream
            memStream.Flush();
            memStream.Close();
            // returning array
            return finalValue;
        }

        /* accessable complete cryptors */
        public static byte[] EncryptMessage(string message)
        {
            UTF8Encoding encoder = new UTF8Encoding();
            byte[] data = encoder.GetBytes(message);
            data = EncryptBase64(data);
            data = EncryptAES(data);
            data = EncryptBase64(data);
            return data;
        }

        public static string DecryptMessage(byte[] message)
        {
            message = DecryptBase64(message);
            message = DecryptAES(message);
            message = DecryptBase64(message);
            return new UTF8Encoding().GetString(message);
        }
    }
}
