﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace ProjectClient
{
    public partial class JoinRoomForm : Form
    {
        private DialogResult res;
        private byte[] buffer;
        private NetworkStream clientStream;
        private List<int> roomIdBySelection;

        public JoinRoomForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            res = DialogResult.Abort;
            this.clientStream = clientStream;
            this.roomIdBySelection = new List<int>();
        }


        private void BackBtn_Click(object sender, EventArgs e)
        {
            res = DialogResult.OK;
            this.Close();
        }

        private void JoinRoomForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
        }

        private void JoinRoomBtn_Click(object sender, EventArgs e)
        {
            int selection = this.RoomListLB.SelectedIndex; // getting user's selected room
            int questionTime = 0, questionCount = 0;
            string msg = "209";
            if(selection == -1) // -1 - not chosen
            {
                MessageBox.Show("Please select a room");
            }
            else
            {
                msg += Helper.GetPaddedNumber(this.roomIdBySelection[this.RoomListLB.SelectedIndex], 4);
                this.clientStream.Write(Encoding.UTF8.GetBytes(msg), 0, 7); // 209 - room list question
                this.clientStream.Flush();
                clientStream.Read(buffer, 0, 4);
                // converting to string (index 3 - response)
                char toCheck = Encoding.UTF8.GetString(this.buffer, 3, 1)[0];
                if (toCheck == '1')
                {
                    MessageBox.Show("room is full");
                }
                else if (toCheck == '2')
                {
                    MessageBox.Show("room not exist");
                }
                else
                {
                    // getting question time and count
                    clientStream.Read(buffer, 0, 4); // questionCount - 2, questionTime 2
                    questionCount = int.Parse(Encoding.UTF8.GetString(buffer, 0, 2));
                    questionTime = int.Parse(Encoding.UTF8.GetString(buffer, 2, 2));
                    // showing the in-room form
                    InRoomForm inRoom = new InRoomForm(
                        this.clientStream, this.roomIdBySelection[this.RoomListLB.SelectedIndex],
                        questionCount, questionTime);
                    this.Hide();
                    // opening window, will return to main form the value got by the in-room form
                    this.res = inRoom.ShowDialog();
                    this.Close();
                    
                }

            }
        }

        private void JoinRoomForm_Load(object sender, EventArgs e)
        {
            UpdateRoomList();   
        }

        /// <summary>
        /// Updates the room list in the list box
        /// </summary>
        private void UpdateRoomList()
        {
            // variables
            int roomCount = 0;
            int roomNameSize = 0;
            string roomName = "";

            // clearing room list
            this.roomIdBySelection.Clear();
            this.RoomListLB.Items.Clear();
            try // - Will crash if server is closed
            {
                // sending the request
                this.clientStream.Write(Encoding.UTF8.GetBytes("205"), 0, 3); // 205 - room list question
                this.clientStream.Flush();
            }
            catch
            {
                this.Close();
            }

            // getting room size
            buffer = new byte[1024];
            this.clientStream.Read(buffer, 0, 3); // reading protocol
            // checking if something went wrong
            if(Encoding.UTF8.GetString(buffer,0,3) != "106")
            {
                this.Close();
            }

            // getting room count
            this.clientStream.Read(buffer, 0, 4); 
            roomCount = int.Parse(Encoding.UTF8.GetString(buffer, 0, 4));
            // getting all rooms
            for(int i = 0; i < roomCount; i++)
            {
                // getting room id
                this.clientStream.Read(buffer, 0, 4);
                int id=int.Parse(Encoding.UTF8.GetString(buffer, 0, 4));
                // getting size of room name
                this.clientStream.Read(buffer, 0, 2); 
                roomNameSize = int.Parse(Encoding.UTF8.GetString(buffer, 0, 2));
                // getting room name and adding to room list
                this.clientStream.Read(buffer, 0, roomNameSize);
                roomName = Encoding.UTF8.GetString(buffer, 0, roomNameSize);
                this.roomIdBySelection.Add(id);
                this.RoomListLB.Items.Add(roomName);
            }
        }

        private void RefreshBtn_Click(object sender, EventArgs e)
        {
            UpdateRoomList();
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
