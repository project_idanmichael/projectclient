﻿namespace ProjectClient
{
    partial class PersonalStatusForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this.numGamesLbl = new System.Windows.Forms.Label();
            this.rightAnsLbl = new System.Windows.Forms.Label();
            this.wrongAndLbl = new System.Windows.Forms.Label();
            this.avgLbl = new System.Windows.Forms.Label();
            this.backBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(817, 92);
            this.HeadlineLbl.TabIndex = 9;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.titleLbl.Location = new System.Drawing.Point(217, 122);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(385, 42);
            this.titleLbl.TabIndex = 10;
            this.titleLbl.Text = "MY PERSONAL STATUS";
            // 
            // numGamesLbl
            // 
            this.numGamesLbl.AutoSize = true;
            this.numGamesLbl.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numGamesLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.numGamesLbl.Location = new System.Drawing.Point(12, 185);
            this.numGamesLbl.Name = "numGamesLbl";
            this.numGamesLbl.Size = new System.Drawing.Size(343, 42);
            this.numGamesLbl.TabIndex = 11;
            this.numGamesLbl.Text = "number of games:";
            // 
            // rightAnsLbl
            // 
            this.rightAnsLbl.AutoSize = true;
            this.rightAnsLbl.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rightAnsLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.rightAnsLbl.Location = new System.Drawing.Point(7, 274);
            this.rightAnsLbl.Name = "rightAnsLbl";
            this.rightAnsLbl.Size = new System.Drawing.Size(506, 42);
            this.rightAnsLbl.TabIndex = 12;
            this.rightAnsLbl.Text = "number of right answers:";
            // 
            // wrongAndLbl
            // 
            this.wrongAndLbl.AutoSize = true;
            this.wrongAndLbl.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wrongAndLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.wrongAndLbl.Location = new System.Drawing.Point(7, 375);
            this.wrongAndLbl.Name = "wrongAndLbl";
            this.wrongAndLbl.Size = new System.Drawing.Size(519, 42);
            this.wrongAndLbl.TabIndex = 13;
            this.wrongAndLbl.Text = "number of wrong answers:";
            // 
            // avgLbl
            // 
            this.avgLbl.AutoSize = true;
            this.avgLbl.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.avgLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.avgLbl.Location = new System.Drawing.Point(7, 469);
            this.avgLbl.Name = "avgLbl";
            this.avgLbl.Size = new System.Drawing.Size(497, 42);
            this.avgLbl.TabIndex = 14;
            this.avgLbl.Text = "average time for answer:";
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.backBtn.FlatAppearance.BorderSize = 0;
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.backBtn.Location = new System.Drawing.Point(266, 618);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(284, 57);
            this.backBtn.TabIndex = 17;
            this.backBtn.Text = "back";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // PersonalStatusForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(817, 698);
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.avgLbl);
            this.Controls.Add(this.wrongAndLbl);
            this.Controls.Add(this.rightAnsLbl);
            this.Controls.Add(this.numGamesLbl);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.HeadlineLbl);
            this.Name = "PersonalStatusForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "PersonalStatusForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PersonalStatusForm_FormClosing);
            this.Load += new System.EventHandler(this.PersonalStatusForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Label numGamesLbl;
        private System.Windows.Forms.Label rightAnsLbl;
        private System.Windows.Forms.Label wrongAndLbl;
        private System.Windows.Forms.Label avgLbl;
        private System.Windows.Forms.Button backBtn;
    }
}