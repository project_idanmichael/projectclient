﻿namespace ProjectClient
{
    partial class JoinRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BackgroundPanel = new System.Windows.Forms.Panel();
            this.RoomListLB = new System.Windows.Forms.ListBox();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.BackBtn = new System.Windows.Forms.Button();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.JoinRoomBtn = new System.Windows.Forms.Button();
            this.RefreshBtn = new System.Windows.Forms.Button();
            this.BackgroundPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackgroundPanel
            // 
            this.BackgroundPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.BackgroundPanel.Controls.Add(this.RoomListLB);
            this.BackgroundPanel.Location = new System.Drawing.Point(132, 154);
            this.BackgroundPanel.Name = "BackgroundPanel";
            this.BackgroundPanel.Size = new System.Drawing.Size(568, 434);
            this.BackgroundPanel.TabIndex = 16;
            // 
            // RoomListLB
            // 
            this.RoomListLB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RoomListLB.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomListLB.FormattingEnabled = true;
            this.RoomListLB.IntegralHeight = false;
            this.RoomListLB.ItemHeight = 25;
            this.RoomListLB.Location = new System.Drawing.Point(14, 17);
            this.RoomListLB.Name = "RoomListLB";
            this.RoomListLB.Size = new System.Drawing.Size(544, 398);
            this.RoomListLB.TabIndex = 0;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.ExitBtn.Location = new System.Drawing.Point(274, 668);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(284, 57);
            this.ExitBtn.TabIndex = 2;
            this.ExitBtn.Text = "Quit Game";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 14;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BackBtn
            // 
            this.BackBtn.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackBtn.Location = new System.Drawing.Point(697, 109);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(124, 38);
            this.BackBtn.TabIndex = 4;
            this.BackBtn.Text = "Go Back";
            this.BackBtn.UseVisualStyleBackColor = true;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click);
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.ForeColor = System.Drawing.Color.White;
            this.UserNameLbl.Location = new System.Drawing.Point(9, 109);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(143, 28);
            this.UserNameLbl.TabIndex = 18;
            this.UserNameLbl.Text = "Name of User";
            // 
            // JoinRoomBtn
            // 
            this.JoinRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.JoinRoomBtn.FlatAppearance.BorderSize = 0;
            this.JoinRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JoinRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JoinRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.JoinRoomBtn.Location = new System.Drawing.Point(274, 594);
            this.JoinRoomBtn.Name = "JoinRoomBtn";
            this.JoinRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.JoinRoomBtn.TabIndex = 1;
            this.JoinRoomBtn.Text = "Join Room";
            this.JoinRoomBtn.UseVisualStyleBackColor = false;
            this.JoinRoomBtn.Click += new System.EventHandler(this.JoinRoomBtn_Click);
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RefreshBtn.Location = new System.Drawing.Point(354, 110);
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.Size = new System.Drawing.Size(124, 38);
            this.RefreshBtn.TabIndex = 3;
            this.RefreshBtn.Text = "Refresh";
            this.RefreshBtn.UseVisualStyleBackColor = true;
            this.RefreshBtn.Click += new System.EventHandler(this.RefreshBtn_Click);
            // 
            // JoinRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 737);
            this.Controls.Add(this.RefreshBtn);
            this.Controls.Add(this.BackgroundPanel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.HeadlineLbl);
            this.Controls.Add(this.BackBtn);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.JoinRoomBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "JoinRoomForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "JoinRoomForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.JoinRoomForm_FormClosing);
            this.Load += new System.EventHandler(this.JoinRoomForm_Load);
            this.BackgroundPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel BackgroundPanel;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Button BackBtn;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Button JoinRoomBtn;
        private System.Windows.Forms.ListBox RoomListLB;
        private System.Windows.Forms.Button RefreshBtn;
    }
}