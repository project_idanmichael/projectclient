﻿namespace ProjectClient
{
    partial class CreateRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.NumQuestionsLbl = new System.Windows.Forms.Label();
            this.NumPlayerLbl = new System.Windows.Forms.Label();
            this.QuestionTimeLbl = new System.Windows.Forms.Label();
            this.RoomNameLbl = new System.Windows.Forms.Label();
            this.QuestionTimeTB = new System.Windows.Forms.TextBox();
            this.NumQuestionsTB = new System.Windows.Forms.TextBox();
            this.NumPlayersTB = new System.Windows.Forms.TextBox();
            this.RoomNameTB = new System.Windows.Forms.TextBox();
            this.CreateRoomBtn = new System.Windows.Forms.Button();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.BackBtn = new System.Windows.Forms.Button();
            this.LoginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 8;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.ExitBtn.Location = new System.Drawing.Point(274, 668);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(284, 57);
            this.ExitBtn.TabIndex = 5;
            this.ExitBtn.Text = "Quit Game";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // LoginPanel
            // 
            this.LoginPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.LoginPanel.Controls.Add(this.NumQuestionsLbl);
            this.LoginPanel.Controls.Add(this.NumPlayerLbl);
            this.LoginPanel.Controls.Add(this.QuestionTimeLbl);
            this.LoginPanel.Controls.Add(this.RoomNameLbl);
            this.LoginPanel.Controls.Add(this.QuestionTimeTB);
            this.LoginPanel.Controls.Add(this.NumQuestionsTB);
            this.LoginPanel.Controls.Add(this.NumPlayersTB);
            this.LoginPanel.Controls.Add(this.RoomNameTB);
            this.LoginPanel.Location = new System.Drawing.Point(120, 149);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(568, 434);
            this.LoginPanel.TabIndex = 999;
            // 
            // NumQuestionsLbl
            // 
            this.NumQuestionsLbl.AutoSize = true;
            this.NumQuestionsLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.NumQuestionsLbl.Font = new System.Drawing.Font("Monotype Corsiva", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumQuestionsLbl.ForeColor = System.Drawing.Color.White;
            this.NumQuestionsLbl.Location = new System.Drawing.Point(147, 222);
            this.NumQuestionsLbl.Name = "NumQuestionsLbl";
            this.NumQuestionsLbl.Size = new System.Drawing.Size(275, 39);
            this.NumQuestionsLbl.TabIndex = 999;
            this.NumQuestionsLbl.Text = "Number of Questions";
            // 
            // NumPlayerLbl
            // 
            this.NumPlayerLbl.AutoSize = true;
            this.NumPlayerLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.NumPlayerLbl.Font = new System.Drawing.Font("Monotype Corsiva", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumPlayerLbl.ForeColor = System.Drawing.Color.White;
            this.NumPlayerLbl.Location = new System.Drawing.Point(162, 111);
            this.NumPlayerLbl.Name = "NumPlayerLbl";
            this.NumPlayerLbl.Size = new System.Drawing.Size(244, 39);
            this.NumPlayerLbl.TabIndex = 999;
            this.NumPlayerLbl.Text = "Number of Players";
            // 
            // QuestionTimeLbl
            // 
            this.QuestionTimeLbl.AutoSize = true;
            this.QuestionTimeLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.QuestionTimeLbl.Font = new System.Drawing.Font("Monotype Corsiva", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionTimeLbl.ForeColor = System.Drawing.Color.White;
            this.QuestionTimeLbl.Location = new System.Drawing.Point(161, 333);
            this.QuestionTimeLbl.Name = "QuestionTimeLbl";
            this.QuestionTimeLbl.Size = new System.Drawing.Size(246, 39);
            this.QuestionTimeLbl.TabIndex = 999;
            this.QuestionTimeLbl.Text = "Time For Question";
            // 
            // RoomNameLbl
            // 
            this.RoomNameLbl.AutoSize = true;
            this.RoomNameLbl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.RoomNameLbl.Font = new System.Drawing.Font("Monotype Corsiva", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomNameLbl.ForeColor = System.Drawing.Color.White;
            this.RoomNameLbl.Location = new System.Drawing.Point(202, 0);
            this.RoomNameLbl.Name = "RoomNameLbl";
            this.RoomNameLbl.Size = new System.Drawing.Size(164, 39);
            this.RoomNameLbl.TabIndex = 999;
            this.RoomNameLbl.Text = "Room Name";
            // 
            // QuestionTimeTB
            // 
            this.QuestionTimeTB.BackColor = System.Drawing.Color.Black;
            this.QuestionTimeTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.QuestionTimeTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QuestionTimeTB.ForeColor = System.Drawing.Color.White;
            this.QuestionTimeTB.Location = new System.Drawing.Point(3, 392);
            this.QuestionTimeTB.MaxLength = 2;
            this.QuestionTimeTB.Name = "QuestionTimeTB";
            this.QuestionTimeTB.Size = new System.Drawing.Size(562, 32);
            this.QuestionTimeTB.TabIndex = 3;
            // 
            // NumQuestionsTB
            // 
            this.NumQuestionsTB.BackColor = System.Drawing.Color.Black;
            this.NumQuestionsTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NumQuestionsTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumQuestionsTB.ForeColor = System.Drawing.Color.White;
            this.NumQuestionsTB.Location = new System.Drawing.Point(3, 281);
            this.NumQuestionsTB.MaxLength = 2;
            this.NumQuestionsTB.Name = "NumQuestionsTB";
            this.NumQuestionsTB.Size = new System.Drawing.Size(562, 32);
            this.NumQuestionsTB.TabIndex = 2;
            // 
            // NumPlayersTB
            // 
            this.NumPlayersTB.BackColor = System.Drawing.Color.Black;
            this.NumPlayersTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NumPlayersTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NumPlayersTB.ForeColor = System.Drawing.Color.White;
            this.NumPlayersTB.Location = new System.Drawing.Point(3, 170);
            this.NumPlayersTB.MaxLength = 1;
            this.NumPlayersTB.Name = "NumPlayersTB";
            this.NumPlayersTB.Size = new System.Drawing.Size(562, 32);
            this.NumPlayersTB.TabIndex = 1;
            // 
            // RoomNameTB
            // 
            this.RoomNameTB.BackColor = System.Drawing.Color.Black;
            this.RoomNameTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RoomNameTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RoomNameTB.ForeColor = System.Drawing.Color.White;
            this.RoomNameTB.Location = new System.Drawing.Point(3, 59);
            this.RoomNameTB.MaxLength = 99;
            this.RoomNameTB.Name = "RoomNameTB";
            this.RoomNameTB.Size = new System.Drawing.Size(562, 32);
            this.RoomNameTB.TabIndex = 0;
            // 
            // CreateRoomBtn
            // 
            this.CreateRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.CreateRoomBtn.FlatAppearance.BorderSize = 0;
            this.CreateRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.CreateRoomBtn.Location = new System.Drawing.Point(274, 589);
            this.CreateRoomBtn.Name = "CreateRoomBtn";
            this.CreateRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.CreateRoomBtn.TabIndex = 4;
            this.CreateRoomBtn.Text = "Create Room";
            this.CreateRoomBtn.UseVisualStyleBackColor = false;
            this.CreateRoomBtn.Click += new System.EventHandler(this.CreateRoomBtn_Click);
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.ForeColor = System.Drawing.Color.White;
            this.UserNameLbl.Location = new System.Drawing.Point(9, 104);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(143, 28);
            this.UserNameLbl.TabIndex = 999;
            this.UserNameLbl.Text = "Name of User";
            // 
            // BackBtn
            // 
            this.BackBtn.Font = new System.Drawing.Font("Perpetua Titling MT", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BackBtn.Location = new System.Drawing.Point(697, 104);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(124, 38);
            this.BackBtn.TabIndex = 6;
            this.BackBtn.Text = "Go Back";
            this.BackBtn.UseVisualStyleBackColor = true;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click);
            // 
            // CreateRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 737);
            this.Controls.Add(this.BackBtn);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.CreateRoomBtn);
            this.Controls.Add(this.LoginPanel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.HeadlineLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CreateRoomForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CreateRoomForm_FormClosing);
            this.Load += new System.EventHandler(this.CreateRoomForm_Load);
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.TextBox NumQuestionsTB;
        private System.Windows.Forms.TextBox NumPlayersTB;
        private System.Windows.Forms.TextBox RoomNameTB;
        private System.Windows.Forms.Label RoomNameLbl;
        private System.Windows.Forms.TextBox QuestionTimeTB;
        private System.Windows.Forms.Button CreateRoomBtn;
        private System.Windows.Forms.Label QuestionTimeLbl;
        private System.Windows.Forms.Label NumQuestionsLbl;
        private System.Windows.Forms.Label NumPlayerLbl;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Button BackBtn;
    }
}

