﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;

namespace ProjectClient
{
    public partial class InGameForm : Form
    {
        private DialogResult res;
        private NetworkStream clientStream;
        private Thread tMessageHandler;
        private byte[] buffer;
        private int questionTime;
        private int questionsLeft;
        private bool toContinue;
        private Button[] answerBtns;
        private System.Windows.Forms.Timer timer;
        private int timeLeft;
        private int myScore;
        
        public InGameForm(NetworkStream clientStream,int questionTime,int questionCount)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            this.timer = new System.Windows.Forms.Timer();
            this.timer.Interval = 1000;
            this.timer.Tick += timer_Tick;
            this.tMessageHandler = new Thread(HandleMessages);
            this.tMessageHandler.IsBackground = true;
            this.buffer = new byte[128];
            this.toContinue = true;
            this.clientStream = clientStream;
            this.questionTime = questionTime;
            this.questionsLeft = questionCount;
            this.myScore = 0;
            // entering all buttons to array
            this.answerBtns = new Button[]{this.Answer0Btn, this.Answer1Btn, this.Answer2Btn, this.Anwer3Btn};
        }

        private void InGameForm_Load(object sender, EventArgs e)
        {
            this.SetData();
            this.tMessageHandler.Start();
        }

        private void InGameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = this.res;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            this.TimeLeftLbl.Text = (--this.timeLeft).ToString();
            Application.DoEvents();

            if(this.timeLeft == 0)
            {
                this.AllowToAnswer(false);
                string msg = "219"; 
                msg += "5" + Helper.GetPaddedNumber(this.questionTime, 2);
                clientStream.Write(Encoding.UTF8.GetBytes(msg), 0, msg.Length);
                clientStream.Flush();
            }
            
        }

        /// <summary>
        /// Resets the timer and sets the time back to 5 seconds
        /// </summary>
        private void ResetTime()
        {
            this.timer.Stop();
            this.timeLeft = this.questionTime;
            this.TimeLeftLbl.Text = this.questionTime.ToString();
            this.timer.Start();
        }
        

        private void HandleMessages()
        {
            
            int code = 0, numUsers;
            string response = "";
            char toCheck;
            while (this.toContinue)
            {
                try
                {
                    this.clientStream.Read(this.buffer, 0, 3); // getting code
                }
                catch
                {
                    toContinue = false;
                    break;
                }
                code = int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 3));
                switch (code)
                {
                    case 118: // questions
                        try
                        {
                            this.SetData();
                        }
                        catch
                        {
                            toContinue = false;
                            Invoke((MethodInvoker)delegate { this.BackBtn.PerformClick(); });
                        }
                        Invoke((MethodInvoker)delegate { this.QuestionsLeftLbl.Text = "Questions Left: " + (--this.questionsLeft).ToString(); });

                        break;
                    case 120: // response to answer
                        this.clientStream.Read(this.buffer, 0, 1);
                        toCheck = Encoding.UTF8.GetString(this.buffer, 0, 1)[0];
                        if (toCheck == '1')
                        {
                            Invoke((MethodInvoker)delegate { this.MyScoreLbl.Text = "Score: " + (++this.myScore).ToString(); });
                        }
                        break;
                    case 121: // game over
                        toContinue = false;
                        this.clientStream.Read(this.buffer, 0, 1);
                        numUsers = int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 1));
                        response = this.GetResults(numUsers);
                        this.timer.Stop();
                        MessageBox.Show(response);
                        this.res = DialogResult.OK;
                        break;
                }
            }
            Invoke((MethodInvoker)delegate { this.BackBtn.PerformClick(); });
        }

        /// <summary>
        /// Sets the new question info on the controls
        /// </summary>
        private void SetData()
        {
            int size;
            // getting the question
            this.clientStream.Read(this.buffer, 0, 3); // getting question size
            size = int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 3));
            // checking if sent error message
            if (size == 0)
            {
                throw new Exception("ERROR : No questions");
            }
            else
            {
                this.clientStream.Read(this.buffer, 0, size);
                //quetion label changing
                Invoke((MethodInvoker)delegate { this.QuestionLbl.Text = Encoding.UTF8.GetString(buffer, 0, size); });
                // getting answers
                for (int i = 0; i < 4; i++)
                {
                    // getting the current answer
                    this.clientStream.Read(this.buffer, 0, 3);
                    size = int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 3));
                    this.clientStream.Read(this.buffer, 0, size);
                    //changing answer's label's text
                    Invoke((MethodInvoker)delegate { this.answerBtns[i].Text = Encoding.UTF8.GetString(this.buffer, 0, size); });
                }
                this.AllowToAnswer(true);
                Invoke((MethodInvoker)delegate { this.ResetTime(); }); // to invoke as well
            }
        }

        
        private void Answer_Click(object sender, EventArgs e)
        {
            string msg = "219";
            int selected_index = ((Button)sender).TabIndex + 1; // tab index is equal to the question index
            this.AllowToAnswer(false);
            msg += selected_index.ToString() + Helper.GetPaddedNumber((this.questionTime - this.timeLeft), 2);
            clientStream.Write(Encoding.UTF8.GetBytes(msg), 0, msg.Length);
            clientStream.Flush();

        }
        
        private string GetResults(int numUsers)
        {
            string res="RESULTS!!\n";
            int size;
            for(int i=0;i<numUsers;i++)
            {
                this.clientStream.Read(this.buffer, 0, 2);
                size = int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 2));
                this.clientStream.Read(this.buffer, 0, size);
                res += Encoding.UTF8.GetString(this.buffer, 0, size);
                this.clientStream.Read(this.buffer, 0, 2);
                res += "-" + int.Parse(Encoding.UTF8.GetString(this.buffer, 0, 2)).ToString() +" points \n";
            }
            return res;   
        }

        /// <summary>
        /// Sets the enabled value of the answer buttons according to the parameter
        /// </summary>
        /// <param name="toAllow">enabled or not</param>
        private void AllowToAnswer(bool toAllow)
        {
            for (int i = 0; i < this.answerBtns.Length; i++)
            {
                Invoke((MethodInvoker)delegate { this.answerBtns[i].Enabled = toAllow; });
            }
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            res = DialogResult.OK;
            this.Close();
        }
        
    }
}
