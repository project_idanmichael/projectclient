﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Drawing;

namespace ProjectClient
{
    public partial class MainForm : Form
    {
        private TcpClient client;
        private NetworkStream clientStream;
        private bool keepConnecting;
		private byte[] buffer;
        private string userName;
		
        public MainForm()
        {
            InitializeComponent();
            AddPlaceHolderEvents();
            this.Location = new Point(20, 0);
            this.BestScoresBtn.Visible = false;
            this.CreateRoomBtn.Visible = false;
            this.JoinRoomBtn.Visible = false;
            this.MyStatusBtn.Visible = false;
            this.SignOutBtn.Visible = false;
            this.buffer = new byte[128];
            this.userName = "";
            keepConnecting = true;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Thread tConnect = new Thread(BeginConnection);
            tConnect.Start();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            keepConnecting = false;
        }

        public void AddPlaceHolderEvents()
        {
            this.UserNameTB.Enter += Helper.TB_Enter;
            this.PasswordTB.Enter += Helper.TB_Enter;

            this.UserNameTB.Leave += Helper.TB_Leave;
            this.PasswordTB.Leave += Helper.TB_Leave;
        }

        private void BeginConnection()
        {
            // trying to connect as long as the program is running
            while(keepConnecting)
            {
                try
                {
                    client = new TcpClient("127.0.0.1", 8820);
                    break;
                }
                catch{};
            }

            // getting the stream if connected
            if (keepConnecting)
            {
                clientStream = client.GetStream();
                //Crypto.CryptoInit(clientStream);
                Invoke((MethodInvoker)delegate { this.LoginPanel.Visible = true;
                                                    this.SignUpBtn.Visible = true;});
            }
        }

        
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

		 /**the function sends a connection request message to the server**/
        private void SignInBtn_Click(object sender, EventArgs e)
        {
            string error = "";
            string msg = "200";
            // Checking for empty inputs
            if (this.UserNameTB.ForeColor == Color.Gray)
            {
                error += "No username input";
            }
            if (this.PasswordTB.ForeColor == Color.Gray)
            {
                error += "No password input";
            }
                // displaying error if found
            if (error != "")
            {
                MessageBox.Show(error);
            }
            else
            {
                // adding the details to the message
                msg += Helper.GetPaddedNumber(this.UserNameTB.Text.Length, 2) + this.UserNameTB.Text.ToLower();
                msg += Helper.GetPaddedNumber(this.PasswordTB.Text.Length, 2) + this.PasswordTB.Text;//creating a sign in message
                // sending message to server
                clientStream.Write(Encoding.UTF8.GetBytes(msg), 0, msg.Length);  
                clientStream.Flush();
                // reading response from server
                clientStream.Read(this.buffer, 0, 4);
                // converting to string (index 3 - response)
                char toCheck = Encoding.UTF8.GetString(this.buffer, 3, 1)[0];
                // checking the response
                if(toCheck=='1') // wrong input
                {
                    MessageBox.Show("wrong input try again");
                }
                else if(toCheck=='2') // already connected
                {
                    MessageBox.Show("this user is already connected");
                }   
                else // correct details
                {
                    // setting username
                    this.userName = this.UserNameTB.Text;
                    // setting to login mode
                    this.nameLbl.Visible = true;
                    this.nameLbl.Text="Hello " + this.UserNameTB.Text;
                    this.nameLbl.Location = new Point(0, 150);
                    this.LoginPanel.Visible = false;
                    this.SignUpBtn.Visible = false;
                    this.SignOutBtn.Visible = true;
                    this.BestScoresBtn.Visible = true;
                    this.CreateRoomBtn.Visible = true;
                    this.JoinRoomBtn.Visible = true;
                    this.MyStatusBtn.Visible = true;
                    this.SignOutBtn.Visible = true;
                }
            }
        }

        private void SignUpBtn_Click(object sender, EventArgs e)
        {
            SignUpForm signUpForm = new SignUpForm(this.clientStream);
            this.Hide();
            if (signUpForm.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void SignOutBtn_Click(object sender, EventArgs e)
        {
            // sending message to server
            clientStream.Write(Encoding.UTF8.GetBytes("201"), 0, 3);  //sending message to server
            clientStream.Flush();
            // switching to guest mode
            this.LoginPanel.Visible = true;
            this.SignUpBtn.Visible = true;
            this.SignOutBtn.Visible = false;
            this.BestScoresBtn.Visible = false;
            this.CreateRoomBtn.Visible = false;
            this.JoinRoomBtn.Visible = false;
            this.MyStatusBtn.Visible = false;
            this.SignOutBtn.Visible = false;
            this.nameLbl.Visible = false;

        }

        private void CreateRoomBtn_Click(object sender, EventArgs e)
        {
            CreateRoomForm createRoomForm = new CreateRoomForm(this.clientStream, this.userName);
            this.Hide();

            if (createRoomForm.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void BestScoresBtn_Click(object sender, EventArgs e)
        {
            //string result,res1="",res2="",res3="";
            string[] results = new string[3];
            int size = 0;//, currIndex=2, userCount = 0;
            // sending message to server
            clientStream.Write(Encoding.UTF8.GetBytes("223"), 0,3);
            clientStream.Flush();
           
            // getting data from client
            clientStream.Read(buffer, 0, 3); // passing the code part
            // checking if a different response was sent
            if(Encoding.UTF8.GetString(buffer, 0, 3) != "124")
            {
                MessageBox.Show("Something went wrong..");
                return;
            }
            // getting users
            for(int i = 0; i < 3; i++)
            {
                // getting user size
                clientStream.Read(buffer, 0, 2);
                size = int.Parse(Encoding.UTF8.GetString(buffer, 0, 2));
                // checking if out of users
                if(size == 0)
                {
                    // completing the empty message (empty = 0x2 for size and 0x6 for results)
                    clientStream.Read(buffer, 0, 6);
                    break;
                }
                // getting username
                clientStream.Read(buffer, 0, size);
                results[i] = Encoding.UTF8.GetString(buffer, 0, size) + " - ";
                // getting score
                clientStream.Read(buffer, 0, 6);
                results[i] += int.Parse(Encoding.UTF8.GetString(buffer, 0, 6));
            }


            BestScoresForm formBestScores = new BestScoresForm(results);
            this.Hide();
            if (formBestScores.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void MyStatusBtn_Click(object sender, EventArgs e)
        {
            string result, numGames = "", rightAnswers = "", wrongAnswers = "", avgAnswer = "";
            // sending message to server
            clientStream.Write(Encoding.UTF8.GetBytes("225"), 0, 3);
            clientStream.Flush();
            // reading response from server
            clientStream.Read(buffer, 0, 23);
            result = Encoding.UTF8.GetString(this.buffer, 3, this.buffer.Length - 3);
            if (!result.Substring(0,4).Equals("0000"))
            {
                numGames = int.Parse(result.Substring(0, 4)).ToString();
                rightAnswers = int.Parse(result.Substring(4, 6)).ToString();
                wrongAnswers = int.Parse(result.Substring(10, 6)).ToString();
                avgAnswer = int.Parse(result.Substring(16, 2)).ToString();
                avgAnswer += ".";
                avgAnswer += int.Parse(result.Substring(18, 2)).ToString();
            }
            else
            {
                numGames = "0";
                rightAnswers = "0";
                wrongAnswers = "0";
                avgAnswer = "0.0";
            }
            PersonalStatusForm personalStatusForm = new PersonalStatusForm(numGames, rightAnswers, wrongAnswers, avgAnswer);
            this.Hide();
            if (personalStatusForm.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        private void JoinRoomBtn_Click(object sender, EventArgs e)
        {
            JoinRoomForm joinRoomForm = new JoinRoomForm(this.clientStream);
            this.Hide();
            if (joinRoomForm.ShowDialog() == System.Windows.Forms.DialogResult.Abort)
            {
                this.Close();
            }
            else
            {
                this.Show();
            }
        }

        // for user pressing enter on login inputs
        private void Login_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                this.SignInBtn.PerformClick();
            }
        }
    }
}

	