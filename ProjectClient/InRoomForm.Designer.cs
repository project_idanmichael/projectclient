﻿namespace ProjectClient
{
    partial class InRoomForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BackgroundPanel = new System.Windows.Forms.Panel();
            this.UserListLB = new System.Windows.Forms.ListBox();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.UserNameLbl = new System.Windows.Forms.Label();
            this.LeaveRoomBtn = new System.Windows.Forms.Button();
            this.StartGameBtn = new System.Windows.Forms.Button();
            this.CloseRoomBtn = new System.Windows.Forms.Button();
            this.BackgroundPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BackgroundPanel
            // 
            this.BackgroundPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.BackgroundPanel.Controls.Add(this.UserListLB);
            this.BackgroundPanel.Location = new System.Drawing.Point(132, 157);
            this.BackgroundPanel.Name = "BackgroundPanel";
            this.BackgroundPanel.Size = new System.Drawing.Size(568, 436);
            this.BackgroundPanel.TabIndex = 23;
            // 
            // UserListLB
            // 
            this.UserListLB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserListLB.Font = new System.Drawing.Font("Modern No. 20", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserListLB.FormattingEnabled = true;
            this.UserListLB.IntegralHeight = false;
            this.UserListLB.ItemHeight = 25;
            this.UserListLB.Location = new System.Drawing.Point(14, 17);
            this.UserListLB.Name = "UserListLB";
            this.UserListLB.Size = new System.Drawing.Size(544, 400);
            this.UserListLB.TabIndex = 0;
            this.UserListLB.TabStop = false;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.ExitBtn.Location = new System.Drawing.Point(274, 668);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(284, 57);
            this.ExitBtn.TabIndex = 2;
            this.ExitBtn.Text = "Quit Game";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 21;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // UserNameLbl
            // 
            this.UserNameLbl.AutoSize = true;
            this.UserNameLbl.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)(((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic) 
                | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameLbl.ForeColor = System.Drawing.Color.White;
            this.UserNameLbl.Location = new System.Drawing.Point(9, 112);
            this.UserNameLbl.Name = "UserNameLbl";
            this.UserNameLbl.Size = new System.Drawing.Size(143, 28);
            this.UserNameLbl.TabIndex = 25;
            this.UserNameLbl.Text = "Name of User";
            // 
            // LeaveRoomBtn
            // 
            this.LeaveRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.LeaveRoomBtn.FlatAppearance.BorderSize = 0;
            this.LeaveRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.LeaveRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LeaveRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.LeaveRoomBtn.Location = new System.Drawing.Point(274, 599);
            this.LeaveRoomBtn.Name = "LeaveRoomBtn";
            this.LeaveRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.LeaveRoomBtn.TabIndex = 24;
            this.LeaveRoomBtn.Text = "Leave Room";
            this.LeaveRoomBtn.UseVisualStyleBackColor = false;
            this.LeaveRoomBtn.Click += new System.EventHandler(this.LeaveRoomBtn_Click);
            // 
            // StartGameBtn
            // 
            this.StartGameBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.StartGameBtn.FlatAppearance.BorderSize = 0;
            this.StartGameBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.StartGameBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartGameBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.StartGameBtn.Location = new System.Drawing.Point(132, 599);
            this.StartGameBtn.Name = "StartGameBtn";
            this.StartGameBtn.Size = new System.Drawing.Size(284, 57);
            this.StartGameBtn.TabIndex = 0;
            this.StartGameBtn.Text = "Start Game";
            this.StartGameBtn.UseVisualStyleBackColor = false;
            this.StartGameBtn.Click += new System.EventHandler(this.StartGameBtn_Click);
            // 
            // CloseRoomBtn
            // 
            this.CloseRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.CloseRoomBtn.FlatAppearance.BorderSize = 0;
            this.CloseRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CloseRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.CloseRoomBtn.Location = new System.Drawing.Point(422, 599);
            this.CloseRoomBtn.Name = "CloseRoomBtn";
            this.CloseRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.CloseRoomBtn.TabIndex = 1;
            this.CloseRoomBtn.Text = "Close Room";
            this.CloseRoomBtn.UseVisualStyleBackColor = false;
            this.CloseRoomBtn.Click += new System.EventHandler(this.CloseRoomBtn_Click);
            // 
            // InRoomForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 737);
            this.Controls.Add(this.CloseRoomBtn);
            this.Controls.Add(this.StartGameBtn);
            this.Controls.Add(this.BackgroundPanel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.HeadlineLbl);
            this.Controls.Add(this.UserNameLbl);
            this.Controls.Add(this.LeaveRoomBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "InRoomForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "InRoomForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InRoomForm_FormClosing);
            this.Load += new System.EventHandler(this.InRoomForm_Load);
            this.BackgroundPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel BackgroundPanel;
        private System.Windows.Forms.ListBox UserListLB;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Label UserNameLbl;
        private System.Windows.Forms.Button LeaveRoomBtn;
        private System.Windows.Forms.Button StartGameBtn;
        private System.Windows.Forms.Button CloseRoomBtn;
    }
}