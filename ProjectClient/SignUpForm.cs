﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;

namespace ProjectClient
{
    public partial class SignUpForm : Form
    {
        private NetworkStream clientStream;
        private DialogResult res;
        private byte[] buffer;
        public SignUpForm(NetworkStream clientStream)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            AddPlaceHolderEvents();
            this.clientStream = clientStream;
            
        }

        public void AddPlaceHolderEvents()
        {
            this.UserNameTB.Enter += Helper.TB_Enter;
            this.PasswordTB.Enter += Helper.TB_Enter;
            this.EmailTB.Enter += Helper.TB_Enter;

            this.UserNameTB.Leave += Helper.TB_Leave;
            this.PasswordTB.Leave += Helper.TB_Leave;
            this.EmailTB.Leave += Helper.TB_Leave;
        }
        private void SignUpForm_Load(object sender, EventArgs e)
        {
            this.res = DialogResult.Abort;
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SignUnBtn_Click(object sender, EventArgs e)
        {
            string error = "";
            string msg = "203";
            // checking for empty inputs
            if (this.UserNameTB.ForeColor == Color.Gray)
            {
                error += "no username input\n";
            }
            if (this.PasswordTB.ForeColor == Color.Gray)
            {
                error += "no password input\n";
            }
            if (this.EmailTB.ForeColor == Color.Gray)
            {
                error += "no email input";
            }
            // displaying error if found
            if (error != "")
            {
                MessageBox.Show(error);
            }
            else
            {
                // building message for server
                msg += Helper.GetPaddedNumber(this.UserNameTB.Text.Length, 2) + this.UserNameTB.Text.ToLower();
                msg += Helper.GetPaddedNumber(this.PasswordTB.Text.Length, 2) + this.PasswordTB.Text;
                msg += Helper.GetPaddedNumber(this.EmailTB.Text.Length, 2) + this.EmailTB.Text;
                this.buffer = Encoding.UTF8.GetBytes(msg); // converting message to bytes
                clientStream.Write(this.buffer, 0, msg.Length);  //sending message to server
                clientStream.Flush();
                // getting response from user
                this.buffer = new byte[4];
                clientStream.Read(buffer, 0, 4); //reading answer of server
                char toCheck = Encoding.UTF8.GetString(this.buffer, 3, 1)[0];

                // checking server response
                switch(toCheck)
                {
                    case '1':
                        MessageBox.Show("Password is illegal");
                        break;
                    case '2':
                        MessageBox.Show("Username already exists");
                        break;
                    case '3':
                        MessageBox.Show("Username is illegal");
                        break;
                    case '4':
                        MessageBox.Show("Unknown error occured");
                        break;
                    default:
                        this.res = DialogResult.OK;
                        this.Close();
                        break;
                }
            }
        }


        private void SignUpForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
        }

        
    }
}
