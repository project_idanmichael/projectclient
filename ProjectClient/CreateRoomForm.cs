﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net.Sockets;

namespace ProjectClient
{
    public partial class CreateRoomForm : Form
    {
        private NetworkStream clientStream;
        private DialogResult res;
        private byte[] buffer;
        private string userName;
        public CreateRoomForm(NetworkStream clientStream, string userName)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            this.buffer = new byte[128];
            this.userName = userName;
            this.clientStream = clientStream;
            res = DialogResult.Abort;
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            res = DialogResult.OK;
            this.Close();
        }

        private void CreateRoomForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
        }

        private void CreateRoomBtn_Click(object sender, EventArgs e)
        {
            int num = 0;
            string error = "";
            string msg = "213";

            /* if = checking if empty | else if = checking if not number */
            // checking for empty inputs
            if (this.RoomNameTB.Text == "" || this.RoomNameTB.Text == null)
            {
                error += "no room name input\n";
            }

            if (this.NumPlayersTB.Text == "" || this.NumPlayersTB.Text == null)
            {
                error += "no number of players input\n";
            }
            else if (!int.TryParse(this.NumPlayersTB.Text, out num))
            {
                error += "only numbers in player count\n";
            }

            if (this.NumQuestionsTB.Text == "" || this.NumQuestionsTB.Text ==null)
            {
                error += "no num of questions input";
            }
            else if (!int.TryParse(this.NumQuestionsTB.Text, out num))
            {
                error += "only numbers in questions count'\n";
            }

            if (this.QuestionTimeTB.Text == "" || this.QuestionTimeTB.Text == null)
            {
                error += "no question time input\n";
            }
            else if (!int.TryParse(this.QuestionTimeTB.Text, out num))
            {
                error += "only numbers in question time\n";
            }
            // displaying error if found
            if (error != "")
            {
                MessageBox.Show(error);
            }
            else
            {
                msg += Helper.GetPaddedNumber(this.RoomNameTB.Text.Length, 2) + this.RoomNameTB.Text;
                msg += this.NumPlayersTB.Text;
                if (!int.TryParse(this.NumQuestionsTB.Text, out num))
                {
                    MessageBox.Show("Question number should be a number!");
                    return;
                }
                msg += Helper.GetPaddedNumber(num, 2);
                if (!int.TryParse(this.QuestionTimeTB.Text, out num))
                {
                    MessageBox.Show("Question time should be a number!");
                }
                msg += Helper.GetPaddedNumber(num, 2);
                // sending message to server
                clientStream.Write(Encoding.UTF8.GetBytes(msg), 0, msg.Length);  //sending message to server
                clientStream.Flush();
                // getting response from user
                clientStream.Read(buffer, 0, 4); //reading answer of server
                char toCheck = Encoding.UTF8.GetString(this.buffer, 3, 1)[0];
                if(toCheck=='1')
                {
                    MessageBox.Show("creating room failed");
                }
                else
                {
                    // showing in-room form
                    InRoomForm inRoom = new InRoomForm(this.clientStream,
                        int.Parse(this.NumQuestionsTB.Text), 
                        int.Parse(this.QuestionTimeTB.Text), this.userName);
                    this.Hide();
                    // opening window, will return to main form the value got by the in-room form
                    this.res = inRoom.ShowDialog();
                    this.Close();
                }
            }
        }

        private void CreateRoomForm_Load(object sender, EventArgs e)
        {
            this.RoomNameTB.Select();
        }


        
    }
}
