﻿namespace ProjectClient
{
    partial class SignUpForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.EmailTB = new System.Windows.Forms.TextBox();
            this.SignUnBtn = new System.Windows.Forms.Button();
            this.PasswordTB = new System.Windows.Forms.TextBox();
            this.UserNameTB = new System.Windows.Forms.TextBox();
            this.LoginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 8;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.ExitBtn.Location = new System.Drawing.Point(274, 668);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(284, 57);
            this.ExitBtn.TabIndex = 9;
            this.ExitBtn.Text = "Exit Game";
            this.ExitBtn.UseVisualStyleBackColor = false;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // LoginPanel
            // 
            this.LoginPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.LoginPanel.Controls.Add(this.EmailTB);
            this.LoginPanel.Controls.Add(this.SignUnBtn);
            this.LoginPanel.Controls.Add(this.PasswordTB);
            this.LoginPanel.Controls.Add(this.UserNameTB);
            this.LoginPanel.Location = new System.Drawing.Point(120, 275);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(568, 136);
            this.LoginPanel.TabIndex = 10;
            // 
            // EmailTB
            // 
            this.EmailTB.BackColor = System.Drawing.Color.Black;
            this.EmailTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EmailTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailTB.ForeColor = System.Drawing.Color.Gray;
            this.EmailTB.Location = new System.Drawing.Point(21, 93);
            this.EmailTB.MaxLength = 99;
            this.EmailTB.Name = "EmailTB";
            this.EmailTB.Size = new System.Drawing.Size(419, 32);
            this.EmailTB.TabIndex = 2;
            this.EmailTB.Text = "Email";
            // 
            // SignUnBtn
            // 
            this.SignUnBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.SignUnBtn.FlatAppearance.BorderSize = 0;
            this.SignUnBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignUnBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUnBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.SignUnBtn.Location = new System.Drawing.Point(461, 0);
            this.SignUnBtn.Name = "SignUnBtn";
            this.SignUnBtn.Size = new System.Drawing.Size(107, 133);
            this.SignUnBtn.TabIndex = 3;
            this.SignUnBtn.Text = "Sign Up";
            this.SignUnBtn.UseVisualStyleBackColor = false;
            this.SignUnBtn.Click += new System.EventHandler(this.SignUnBtn_Click);
            // 
            // PasswordTB
            // 
            this.PasswordTB.BackColor = System.Drawing.Color.Black;
            this.PasswordTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PasswordTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordTB.ForeColor = System.Drawing.Color.Gray;
            this.PasswordTB.Location = new System.Drawing.Point(21, 53);
            this.PasswordTB.MaxLength = 99;
            this.PasswordTB.Name = "PasswordTB";
            this.PasswordTB.Size = new System.Drawing.Size(419, 32);
            this.PasswordTB.TabIndex = 1;
            this.PasswordTB.Text = "Password";
            // 
            // UserNameTB
            // 
            this.UserNameTB.BackColor = System.Drawing.Color.Black;
            this.UserNameTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserNameTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameTB.ForeColor = System.Drawing.Color.Gray;
            this.UserNameTB.Location = new System.Drawing.Point(21, 13);
            this.UserNameTB.MaxLength = 99;
            this.UserNameTB.Name = "UserNameTB";
            this.UserNameTB.Size = new System.Drawing.Size(419, 32);
            this.UserNameTB.TabIndex = 0;
            this.UserNameTB.Text = "UserName";
            // 
            // SignUpForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 737);
            this.Controls.Add(this.LoginPanel);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.HeadlineLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SignUpForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SignUpForm_FormClosing);
            this.Load += new System.EventHandler(this.SignUpForm_Load);
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.TextBox EmailTB;
        private System.Windows.Forms.Button SignUnBtn;
        private System.Windows.Forms.TextBox PasswordTB;
        private System.Windows.Forms.TextBox UserNameTB;
    }
}

