﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Threading;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;

namespace ProjectClient
{
    public partial class InRoomForm : Form
    {
        private Thread tCheckUsers;
        private NetworkStream clientStream;
        private DialogResult res;
        private byte[] buffer;
        private int roomId, questionCount, questionTime; // time in sec
        private string adminName;
        private bool toContinue; // continue checking for updated user list

        // admin creation constructor
        private InRoomForm(NetworkStream clientStream, int questionCount, int questionTime)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            this.clientStream = clientStream;
            this.questionCount = questionCount;
            this.questionTime = questionTime;
            this.toContinue = true;
            this.buffer = new byte[1024];
            this.res = DialogResult.Abort;
        }
        public InRoomForm(NetworkStream clientStream, int questionCount, int questionTime, string adminName) : this(clientStream, questionCount, questionTime)
        {   
            this.adminName = adminName;
            // only admin can start the game or close the room
            this.UserListLB.Items.Add(adminName);
        }
        // user joining constructor
        public InRoomForm(NetworkStream clientStream, int roomId, int questionCount, int questionTime) : this(clientStream, questionCount, questionTime)
        {
            // only admin can start the game or close the room
            this.LeaveRoomBtn.Visible = true;
            this.CloseRoomBtn.Visible = false;
            this.StartGameBtn.Visible = false;
        }


        private void InRoomForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
            // shutting down thread
            toContinue = false;
            if (tCheckUsers.IsAlive)
            {
                this.tCheckUsers.Join();
            }
        }

        private void StartGameBtn_Click(object sender, EventArgs e)
        {
            // sending message to server
            this.clientStream.Write(Encoding.UTF8.GetBytes("217"), 0, 3);
            this.clientStream.Flush();
        }

        private void InRoomForm_Load(object sender, EventArgs e)
        {
           
            this.tCheckUsers = new Thread(UpdateUserList);
            this.tCheckUsers.IsBackground = true;
            this.tCheckUsers.Start();
        }

        /// <summary>
        /// Updates the room list in the list box
        /// </summary>
        private void UpdateUserList()
        {
            // continues trying to get user list until form closing
            while (this.toContinue)
            {
                try
                {
                    HandleBackground();
                }
                catch
                {
                    if (toContinue) // crashed for error
                    {
                        Invoke((MethodInvoker)this.Close);
                    }
                    else // crashed because of form closing
                    {
                        break;
                    }
                }
            }
            this.DialogResult = this.res; 
        }


        /// <summary>
        /// Gets the users from the server and adds them to the list
        /// </summary>
        private void HandleBackground()
        {   
            // variables
            int userCount = 0;
            int userNameSize = 0;
            string userName = "";
            int codeReceived = 0;
            InGameForm inGameForm;

            // getting room size
            clientStream.Read(buffer, 0, 3); // reading protocol
            // resetting list
            Invoke((MethodInvoker)delegate { this.UserListLB.Items.Clear(); });
            
            codeReceived = int.Parse(Encoding.UTF8.GetString(buffer, 0, 3));

            switch(codeReceived)
            {
                // checking for update response
                case 108:
                    // getting user count - 1 byte
                    this.clientStream.Read(buffer, 0, 1);
                    userCount = int.Parse(Encoding.UTF8.GetString(buffer, 0, 1));
                    // getting all users
                    for (int i = 0; i < userCount; i++)
                    {
                        // getting size of username
                        clientStream.Read(buffer, 0, 2);
                        userNameSize = int.Parse(Encoding.UTF8.GetString(buffer, 0, 2));
                        // getting room name and adding to room list
                        clientStream.Read(buffer, 0, userNameSize);
                        userName = Encoding.UTF8.GetString(buffer, 0, userNameSize);
                        Invoke((MethodInvoker)delegate { this.UserListLB.Items.Add(userName); });
                    }
                    break;

                /* checking for exiting responses 
                       (116 - close room, no additional after protocol) */
                case 116:
                    this.res = DialogResult.OK;
                    break;
                case 112: // 112 - exit room response
                    try // reading the '0' of success
                    {
                        this.clientStream.Read(buffer, 0, 1);
                        this.res = DialogResult.OK; // intended exit
                    }
                    catch{}
                    break;
                case 118: // 118 - game started
                    if(this.toContinue)
                    {
                        this.toContinue = false;
                        // opening the game form
                        Invoke((MethodInvoker)delegate { this.Hide(); });
                        inGameForm = new InGameForm(this.clientStream, this.questionTime, this.questionCount);
                        this.res = inGameForm.ShowDialog();
                    }
                    else // in case of tried to exit
                    {
                        try
                        {
                            toContinue = false;
                            this.clientStream.Read(buffer, 0, buffer.Length); // clears socket
                            this.clientStream.Write(Encoding.UTF8.GetBytes("222"), 0, 3); // sends a leave game request
                            this.res = DialogResult.OK;
                        }
                        catch{}
                    }
                    break;
                default:
                    toContinue = false;
                    break;
            }
        }

        private void LeaveRoomBtn_Click(object sender, EventArgs e)
        {
            toContinue = false;
            try // - Will crash if server is closed
            {
                // sending the request
                this.clientStream.Write(Encoding.UTF8.GetBytes("211"), 0, 3); // 211 - leave room request
                this.clientStream.Flush();
            }
            catch
            {
                this.Close();
            }
        }

        private void ExitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CloseRoomBtn_Click(object sender, EventArgs e)
        {
            toContinue = false;
            try // - Will crash if server is closed
            {
                // sending the request
                this.clientStream.Write(Encoding.UTF8.GetBytes("215"), 0, 3); // 215 - close room request
                this.clientStream.Flush();
            }
            catch
            {
                this.Close();
            }
        }

    }
}
