﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectClient
{
    public partial class BestScoresForm : Form
    {
        private DialogResult res;
        public BestScoresForm(string[] results)
        {
            InitializeComponent();
            this.Location = new Point(20, 0);
            this.firstName.Text = results[0];
            this.secondName.Text = results[1];
            this.thirdName.Text = results[2];
        }

        private void BestScoresForm_Load(object sender, EventArgs e)
        {
            this.res = DialogResult.Abort;
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            this.res = DialogResult.OK;
            this.Close();
        }

        private void BestScoresForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.DialogResult = res;
        }
    }
}
