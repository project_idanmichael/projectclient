﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Drawing;

namespace ProjectClient
{
    static class Helper
    {
        public static string GetPaddedNumber(int num,int bytes)
        {
            int numOfDigits=num.ToString().Length;
            return num.ToString().PadLeft(bytes, '0');
        }

        /* Input place holders */
        public static void TB_Enter(object sender, EventArgs e)
        {
            TextBox tb = (TextBox)sender;
            // checking if control is in info mode (so gray color)
            if (tb.ForeColor == Color.Gray)
            {
                tb.Text = "";
                tb.ForeColor = Color.White;
                // setting password char if password tb
                if (tb.Name == "PasswordTB")
                {
                    tb.PasswordChar = '*';
                }
            }
        }

        public static void TB_Leave(object sender, EventArgs e)   
        {
            TextBox tb = (TextBox)sender;
            // returning the textbox to info mode if empty
            if (String.IsNullOrWhiteSpace(tb.Text))
            {
                tb.Text = tb.Name.Substring(0, tb.Name.Length - 2); // getting the name of the tb without the "TB"
                tb.ForeColor = Color.Gray;
                // removing the password char if password tb
                if (tb.Name == "PasswordTB")
                {
                    tb.PasswordChar = '\0';
                }
            }
        }

        /* - Input place holders */

    }
}
