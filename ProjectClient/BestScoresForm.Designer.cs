﻿namespace ProjectClient
{
    partial class BestScoresForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.firstLbl = new System.Windows.Forms.Label();
            this.secondLbl = new System.Windows.Forms.Label();
            this.thirdLbl = new System.Windows.Forms.Label();
            this.firstName = new System.Windows.Forms.Label();
            this.secondName = new System.Windows.Forms.Label();
            this.thirdName = new System.Windows.Forms.Label();
            this.backBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(817, 92);
            this.HeadlineLbl.TabIndex = 9;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // firstLbl
            // 
            this.firstLbl.AutoSize = true;
            this.firstLbl.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.firstLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.firstLbl.Location = new System.Drawing.Point(126, 188);
            this.firstLbl.Name = "firstLbl";
            this.firstLbl.Size = new System.Drawing.Size(57, 26);
            this.firstLbl.TabIndex = 10;
            this.firstLbl.Text = "1st:";
            // 
            // secondLbl
            // 
            this.secondLbl.AutoSize = true;
            this.secondLbl.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.secondLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.secondLbl.Location = new System.Drawing.Point(126, 364);
            this.secondLbl.Name = "secondLbl";
            this.secondLbl.Size = new System.Drawing.Size(61, 26);
            this.secondLbl.TabIndex = 11;
            this.secondLbl.Text = "2nd:";
            // 
            // thirdLbl
            // 
            this.thirdLbl.AutoSize = true;
            this.thirdLbl.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.thirdLbl.ForeColor = System.Drawing.Color.LightGreen;
            this.thirdLbl.Location = new System.Drawing.Point(130, 551);
            this.thirdLbl.Name = "thirdLbl";
            this.thirdLbl.Size = new System.Drawing.Size(61, 26);
            this.thirdLbl.TabIndex = 12;
            this.thirdLbl.Text = "3rd:";
            // 
            // firstName
            // 
            this.firstName.AutoSize = true;
            this.firstName.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.firstName.ForeColor = System.Drawing.Color.LightGreen;
            this.firstName.Location = new System.Drawing.Point(193, 188);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(0, 26);
            this.firstName.TabIndex = 13;
            // 
            // secondName
            // 
            this.secondName.AutoSize = true;
            this.secondName.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.secondName.ForeColor = System.Drawing.Color.LightGreen;
            this.secondName.Location = new System.Drawing.Point(193, 364);
            this.secondName.Name = "secondName";
            this.secondName.Size = new System.Drawing.Size(0, 26);
            this.secondName.TabIndex = 14;
            // 
            // thirdName
            // 
            this.thirdName.AutoSize = true;
            this.thirdName.Font = new System.Drawing.Font("Stencil", 16.25F);
            this.thirdName.ForeColor = System.Drawing.Color.LightGreen;
            this.thirdName.Location = new System.Drawing.Point(193, 551);
            this.thirdName.Name = "thirdName";
            this.thirdName.Size = new System.Drawing.Size(0, 26);
            this.thirdName.TabIndex = 15;
            // 
            // backBtn
            // 
            this.backBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.backBtn.FlatAppearance.BorderSize = 0;
            this.backBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.backBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.backBtn.Location = new System.Drawing.Point(266, 629);
            this.backBtn.Name = "backBtn";
            this.backBtn.Size = new System.Drawing.Size(284, 57);
            this.backBtn.TabIndex = 16;
            this.backBtn.Text = "back";
            this.backBtn.UseVisualStyleBackColor = false;
            this.backBtn.Click += new System.EventHandler(this.backBtn_Click);
            // 
            // BestScoresForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(817, 698);
            this.ControlBox = false;
            this.Controls.Add(this.backBtn);
            this.Controls.Add(this.thirdName);
            this.Controls.Add(this.secondName);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.thirdLbl);
            this.Controls.Add(this.secondLbl);
            this.Controls.Add(this.firstLbl);
            this.Controls.Add(this.HeadlineLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BestScoresForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "BestScoresForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.BestScoresForm_FormClosing);
            this.Load += new System.EventHandler(this.BestScoresForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Label firstLbl;
        private System.Windows.Forms.Label secondLbl;
        private System.Windows.Forms.Label thirdLbl;
        private System.Windows.Forms.Label firstName;
        private System.Windows.Forms.Label secondName;
        private System.Windows.Forms.Label thirdName;
        private System.Windows.Forms.Button backBtn;
    }
}