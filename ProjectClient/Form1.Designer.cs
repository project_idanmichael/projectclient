﻿namespace ProjectClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoginPanel = new System.Windows.Forms.Panel();
            this.UserNameTB = new System.Windows.Forms.TextBox();
            this.PasswordTB = new System.Windows.Forms.TextBox();
            this.SignInBtn = new System.Windows.Forms.Button();
            this.SignUpBtn = new System.Windows.Forms.Button();
            this.JoinRoomBtn = new System.Windows.Forms.Button();
            this.CreateRoomBtn = new System.Windows.Forms.Button();
            this.MyStatusBtn = new System.Windows.Forms.Button();
            this.BestScoresBtn = new System.Windows.Forms.Button();
            this.HeadlineLbl = new System.Windows.Forms.Label();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.SignOutBtn = new System.Windows.Forms.Button();
            this.LoginPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoginPanel
            // 
            this.LoginPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(200)))));
            this.LoginPanel.Controls.Add(this.SignInBtn);
            this.LoginPanel.Controls.Add(this.PasswordTB);
            this.LoginPanel.Controls.Add(this.UserNameTB);
            this.LoginPanel.Location = new System.Drawing.Point(132, 112);
            this.LoginPanel.Name = "LoginPanel";
            this.LoginPanel.Size = new System.Drawing.Size(568, 100);
            this.LoginPanel.TabIndex = 0;
            // 
            // UserNameTB
            // 
            this.UserNameTB.BackColor = System.Drawing.Color.Black;
            this.UserNameTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UserNameTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UserNameTB.ForeColor = System.Drawing.Color.White;
            this.UserNameTB.Location = new System.Drawing.Point(21, 13);
            this.UserNameTB.MaxLength = 99;
            this.UserNameTB.Name = "UserNameTB";
            this.UserNameTB.Size = new System.Drawing.Size(419, 32);
            this.UserNameTB.TabIndex = 0;
            // 
            // PasswordTB
            // 
            this.PasswordTB.BackColor = System.Drawing.Color.Black;
            this.PasswordTB.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PasswordTB.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordTB.ForeColor = System.Drawing.Color.White;
            this.PasswordTB.Location = new System.Drawing.Point(21, 56);
            this.PasswordTB.MaxLength = 99;
            this.PasswordTB.Name = "PasswordTB";
            this.PasswordTB.PasswordChar = '*';
            this.PasswordTB.Size = new System.Drawing.Size(419, 32);
            this.PasswordTB.TabIndex = 1;
            // 
            // SignInBtn
            // 
            this.SignInBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.SignInBtn.FlatAppearance.BorderSize = 0;
            this.SignInBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignInBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignInBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.SignInBtn.Location = new System.Drawing.Point(461, 0);
            this.SignInBtn.Name = "SignInBtn";
            this.SignInBtn.Size = new System.Drawing.Size(107, 100);
            this.SignInBtn.TabIndex = 2;
            this.SignInBtn.Text = "Sign In";
            this.SignInBtn.UseVisualStyleBackColor = false;
            // 
            // SignUpBtn
            // 
            this.SignUpBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.SignUpBtn.FlatAppearance.BorderSize = 0;
            this.SignUpBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignUpBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUpBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.SignUpBtn.Location = new System.Drawing.Point(274, 248);
            this.SignUpBtn.Name = "SignUpBtn";
            this.SignUpBtn.Size = new System.Drawing.Size(284, 57);
            this.SignUpBtn.TabIndex = 4;
            this.SignUpBtn.Text = "Sign Up";
            this.SignUpBtn.UseVisualStyleBackColor = false;
            // 
            // JoinRoomBtn
            // 
            this.JoinRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.JoinRoomBtn.FlatAppearance.BorderSize = 0;
            this.JoinRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JoinRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JoinRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.JoinRoomBtn.Location = new System.Drawing.Point(274, 341);
            this.JoinRoomBtn.Name = "JoinRoomBtn";
            this.JoinRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.JoinRoomBtn.TabIndex = 5;
            this.JoinRoomBtn.Text = "Join Room";
            this.JoinRoomBtn.UseVisualStyleBackColor = false;
            // 
            // CreateRoomBtn
            // 
            this.CreateRoomBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.CreateRoomBtn.FlatAppearance.BorderSize = 0;
            this.CreateRoomBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateRoomBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateRoomBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.CreateRoomBtn.Location = new System.Drawing.Point(274, 434);
            this.CreateRoomBtn.Name = "CreateRoomBtn";
            this.CreateRoomBtn.Size = new System.Drawing.Size(284, 57);
            this.CreateRoomBtn.TabIndex = 6;
            this.CreateRoomBtn.Text = "Create Room";
            this.CreateRoomBtn.UseVisualStyleBackColor = false;
            // 
            // MyStatusBtn
            // 
            this.MyStatusBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.MyStatusBtn.FlatAppearance.BorderSize = 0;
            this.MyStatusBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MyStatusBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyStatusBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.MyStatusBtn.Location = new System.Drawing.Point(274, 527);
            this.MyStatusBtn.Name = "MyStatusBtn";
            this.MyStatusBtn.Size = new System.Drawing.Size(284, 57);
            this.MyStatusBtn.TabIndex = 7;
            this.MyStatusBtn.Text = "My Status";
            this.MyStatusBtn.UseVisualStyleBackColor = false;
            // 
            // BestScoresBtn
            // 
            this.BestScoresBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.BestScoresBtn.FlatAppearance.BorderSize = 0;
            this.BestScoresBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BestScoresBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BestScoresBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.BestScoresBtn.Location = new System.Drawing.Point(274, 620);
            this.BestScoresBtn.Name = "BestScoresBtn";
            this.BestScoresBtn.Size = new System.Drawing.Size(284, 57);
            this.BestScoresBtn.TabIndex = 8;
            this.BestScoresBtn.Text = "Best Scores";
            this.BestScoresBtn.UseVisualStyleBackColor = false;
            // 
            // HeadlineLbl
            // 
            this.HeadlineLbl.BackColor = System.Drawing.Color.Green;
            this.HeadlineLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.HeadlineLbl.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.HeadlineLbl.Font = new System.Drawing.Font("Elephant", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HeadlineLbl.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeadlineLbl.Location = new System.Drawing.Point(0, 0);
            this.HeadlineLbl.Name = "HeadlineLbl";
            this.HeadlineLbl.Size = new System.Drawing.Size(833, 92);
            this.HeadlineLbl.TabIndex = 8;
            this.HeadlineLbl.Text = "M I N E C R A F T";
            this.HeadlineLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ExitBtn
            // 
            this.ExitBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(150)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.ExitBtn.FlatAppearance.BorderSize = 0;
            this.ExitBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExitBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.ExitBtn.Location = new System.Drawing.Point(274, 713);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(284, 57);
            this.ExitBtn.TabIndex = 9;
            this.ExitBtn.Text = "Quit Game";
            this.ExitBtn.UseVisualStyleBackColor = false;
            // 
            // SignOutBtn
            // 
            this.SignOutBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(150)))));
            this.SignOutBtn.FlatAppearance.BorderSize = 0;
            this.SignOutBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SignOutBtn.Font = new System.Drawing.Font("Stencil", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignOutBtn.ForeColor = System.Drawing.Color.LightGreen;
            this.SignOutBtn.Location = new System.Drawing.Point(274, 248);
            this.SignOutBtn.Name = "SignOutBtn";
            this.SignOutBtn.Size = new System.Drawing.Size(284, 57);
            this.SignOutBtn.TabIndex = 3;
            this.SignOutBtn.Text = "Sign Out";
            this.SignOutBtn.UseVisualStyleBackColor = false;
            this.SignOutBtn.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(833, 782);
            this.Controls.Add(this.SignOutBtn);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.HeadlineLbl);
            this.Controls.Add(this.BestScoresBtn);
            this.Controls.Add(this.MyStatusBtn);
            this.Controls.Add(this.CreateRoomBtn);
            this.Controls.Add(this.JoinRoomBtn);
            this.Controls.Add(this.SignUpBtn);
            this.Controls.Add(this.LoginPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.LoginPanel.ResumeLayout(false);
            this.LoginPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LoginPanel;
        private System.Windows.Forms.Button SignInBtn;
        private System.Windows.Forms.TextBox PasswordTB;
        private System.Windows.Forms.TextBox UserNameTB;
        private System.Windows.Forms.Button SignUpBtn;
        private System.Windows.Forms.Button JoinRoomBtn;
        private System.Windows.Forms.Button CreateRoomBtn;
        private System.Windows.Forms.Button MyStatusBtn;
        private System.Windows.Forms.Button BestScoresBtn;
        private System.Windows.Forms.Label HeadlineLbl;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.Button SignOutBtn;
    }
}

